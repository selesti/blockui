/*jslint browser: true*/

//Block UI JS
(function ($) {

    'use strict';

    $.fn.blockui = $.fn.blockUI = function () {
        this.addClass('block-ui-target block-ui-block');
        return this;
    };

    $.fn.unblockui = $.fn.unblockUI = function () {
        this.removeClass('block-ui-target block-ui-block');
        return this;
    };

}(window.jQuery));

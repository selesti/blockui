# BlockUI Lite
> Allows simply UI blocking for basic tasks like ajax contact forms etc.

## Installation
- run `$ bower install selesti/blockui --save`
- include `blockui.js` and `blockui.css` into your project

## Usage
- Block the element `jQuery('body').blockUI();`
- Unblock the element `jQuery('body').unblockUI();`
